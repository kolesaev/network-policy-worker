package main

import (
	"fmt"
	"os"

	"gitlab.com/kolesaev/network-policy-worker/pkg/cmd"
)

func main() {
	err := cmd.Execute()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
