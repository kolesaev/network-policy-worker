package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/kolesaev/network-policy-worker/pkg/k8s"
	"gitlab.com/kolesaev/network-policy-worker/pkg/vaultengine"
)

var rootCmd = &cobra.Command{
	Use: "network-policy-worker",
	RunE: func(cmd *cobra.Command, args []string) error {
		// Apply the viper config value to the flag when the flag is not set and viper has a value
		address, _ := cmd.Flags().GetString("address")
		if viper.IsSet("VAULT_ADDR") && address == "" {
			value := viper.Get("VAULT_ADDR").(string)
			err := cmd.Flags().Set("address", value)
			if err != nil {
				return err
			}
		}

		token, _ := cmd.Flags().GetString("token")
		if viper.IsSet("VAULT_TOKEN") && token == "" {
			value := viper.Get("VAULT_TOKEN").(string)
			err := cmd.Flags().Set("token", value)
			if err != nil {
				return err
			}
		}

		insecure, _ := cmd.Flags().GetBool("insecure")
		if viper.IsSet("VAULT_SKIP_VERIFY") && insecure == false {
			value := viper.GetBool("VAULT_SKIP_VERIFY")
			err := cmd.Flags().Set("insecure", strconv.FormatBool(value))
			if err != nil {
				return err
			}
		}

		engineType, _ := cmd.Flags().GetString("engine-type")
		path, _ := cmd.Flags().GetString("path")
		key, _ := cmd.Flags().GetString("key")
		namespace, _ := cmd.Flags().GetString("namespace")
		kubeconfig, _ := cmd.Flags().GetString("kubeconfig")

		// Setup Vault client
		client := vaultengine.NewClient(address, token, insecure, namespace)
		engine, path, err := client.MountpathSplitPrefix(path)
		if err != nil {
			fmt.Println(err)
			return err
		}

		client.UseEngine(engine)
		client.SetEngineType(engineType)

		vaultResult := client.SecretRead(path)
		resultValue := vaultResult[key].(string)

		fmt.Println(resultValue)

		k8s.Get(kubeconfig, namespace)

		return nil
	},
}

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	rootCmd.PersistentFlags().StringP("address", "a", "", "Address of the Vault server")
	rootCmd.PersistentFlags().StringP("token", "t", "", "Vault authentication token")
	rootCmd.PersistentFlags().StringP("namespace", "n", "", "K8s namespace")
	rootCmd.PersistentFlags().BoolP("insecure", "i", false, "Allow insecure server connections when using SSL")
	rootCmd.PersistentFlags().StringP("engine-type", "m", "kv2", "Specify the secret engine type [kv1|kv2]")
	rootCmd.PersistentFlags().StringP("key", "k", "", "Key name of the value")
	rootCmd.PersistentFlags().StringP("path", "p", "", "Path to the value")
	rootCmd.PersistentFlags().StringP("kubeconfig", "c", "", "Path to the kubeconfig")

	// AutomaticEnv makes Viper load environment variables
	viper.AutomaticEnv()

	// Explicitly defines the path, name and type of the config file.
	// Viper will use this and not check any of the config paths.
	// It will search for the "config" file in the ~/.network-policy-worker
	viper.SetConfigType("yaml")
	viper.AddConfigPath("$HOME/.network-policy-worker")
	viper.SetConfigName("config")

	// Find and read the config file
	err := viper.ReadInConfig()

	if err != nil {
		// log.Fatalf("Error while reading config file %s", err)
	}

}
