package k8s

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"

	apiv1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/retry"
)

func Get(kubeconfigpath string, namespace string) {
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfigpath)
	if err != nil {
		panic(err)
	}
	log.Println(config)
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	var protocol apiv1.Protocol
	var port intstr.IntOrString
	protocol = "TCP"
	port = intstr.IntOrString{
		Type:   intstr.Int,
		IntVal: 8080,
	}

	client := clientset.NetworkingV1().NetworkPolicies(namespace)

	np := &netv1.NetworkPolicy{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "demo-policy",
			Namespace: namespace,
		},
		Spec: netv1.NetworkPolicySpec{
			PolicyTypes: []netv1.PolicyType{
				netv1.PolicyTypeIngress,
			},
			Ingress: []netv1.NetworkPolicyIngressRule{
				{
					Ports: []netv1.NetworkPolicyPort{
						{
							Protocol: &protocol,
							Port:     &port,
						},
					},
					From: []netv1.NetworkPolicyPeer{
						{
							IPBlock: &netv1.IPBlock{
								CIDR: "0.0.0.0/0",
							},
						},
					},
				},
			},
			PodSelector: metav1.LabelSelector{
				MatchLabels: map[string]string{
					"app": "ubuntu",
				},
			},
		},
	}

	// Create network policy
	fmt.Println("Creating ...")
	result, err := client.Create(context.TODO(), np, metav1.CreateOptions{})
	if err != nil {
		panic(err)
	}
	fmt.Printf("Created network policy %q.\n", result.GetObjectMeta().GetName())

	// Update network policy
	prompt()
	fmt.Println("Updating network policy...")
	//    You have two options to Update() this network policy:
	//
	//    1. Modify the "network policy" variable and call: Update(network policy).
	//       This works like the "kubectl replace" command and it overwrites/loses changes
	//       made by other clients between you Create() and Update() the object.
	//    2. Modify the "result" returned by Get() and retry Update(result) until
	//       you no longer get a conflict error. This way, you can preserve changes made
	//       by other clients between Create() and Update(). This is implemented below
	//			 using the retry utility package included with client-go. (RECOMMENDED)
	//
	// More Info:
	// https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#concurrency-control-and-consistency

	retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {
		// Retrieve the latest version of network policy before attempting update
		// RetryOnConflict uses exponential backoff to avoid exhausting the apiserver
		result, getErr := client.Get(context.TODO(), "demo-policy", metav1.GetOptions{})
		if getErr != nil {
			panic(fmt.Errorf("Failed to get latest version of network policy: %v", getErr))
		}

		result.Spec.Ingress[0].From[0].IPBlock.CIDR = "10.0.0.0/16" // reduce replica count
		_, updateErr := client.Update(context.TODO(), result, metav1.UpdateOptions{})
		return updateErr
	})
	if retryErr != nil {
		panic(fmt.Errorf("Update failed: %v", retryErr))
	}
	fmt.Println("Updated network policy...")

	// List network policies
	prompt()
	fmt.Printf("Listing network policies in namespace %q:\n", namespace)
	list, err := client.List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	for _, d := range list.Items {
		fmt.Printf(" * %s (%d replicas)\n", d.Name, *&d.Spec.Ingress[0].Ports[0].Port)
	}

	// Delete network policy
	prompt()
	fmt.Println("Deleting network policy...")
	deletePolicy := metav1.DeletePropagationForeground
	if err := client.Delete(context.TODO(), "demo-policy", metav1.DeleteOptions{
		PropagationPolicy: &deletePolicy,
	}); err != nil {
		panic(err)
	}
	fmt.Println("Deleted network policy.")
}

func prompt() {
	fmt.Printf("-> Press Return key to continue.")
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		break
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}
	fmt.Println()
}

func int32Ptr(i int32) *int32 { return &i }
